#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct node
{
    long long val;
    struct node *next;
} node;

void prob1(); //APPROVED Flowchart done
void prob3(); //REQUIRED
void prob4(); //REQUIRED
void prob5(); //REQUIRED
void prob6(); //REQUIRED Flowchart done
void prob7(); //APPROVED
void prob9(); //APPROVED Flowchart done
void prob11(); //REQUIRED
void prob12(); //APPROVED Flowchart done
void prob14(); //APPROVED

long long llGcd(long long a, long long b);
long long llSqrt(long long n);
void add_to_int_arr(int **arr, int n, int *i, int *capacity);
void add_to_ll_arr(long long **arr, long long n, int *i, int *capacity);
int all_bits_one(int bits[], int n);
void print_bits(int bits[], int n);
node *egypt_brute_force(long long p, long long q);
node *egypt_two_terms(long long p, long long q);
node *add_to_top(node *head, long long val);
void free_list(node *head);

int main(void)
{
    int input;
    do
    {
        printf("\n");
        printf("0:  Exit program\n");
        printf("1:  No. of rightmost 0s of n!\n");
        printf("3:  Prime factors\n");
        printf("4:  All possible subsets\n");
        printf("5:  Polynomial product\n");
        printf("6:  nth lightbulb\n");
        printf("7:  Lowest-term fractions with denominator n\n");
        printf("9:  nth row of Pascal's triangle\n");
        printf("11: Egyptian fraction sum\n");
        printf("12: Express n as a set of bills and coins\n");
        printf("14: Base converter\n");
        printf("Enter problem number: ");
        scanf("%d", &input);
        switch(input)
        {
            case 1:
                prob1();
                break;
            case 3:
                prob3();
                break;
            case 4:
                prob4();
                break;
            case 5:
                prob5();
                break;
            case 6:
                prob6();
                break;
            case 7:
                prob7();
                break;
            case 9:
                prob9();
                break;
            case 11:
                prob11();
                break;
            case 12:
                prob12();
                break;
            case 14:
                prob14();
                break;
        }
        if (input)
            usleep(1000000);
    } while (input);
    printf("Exiting program.\n");
    return 0;
}

void prob1(void)
{
    long long n;
    printf("Enter number: ");
    scanf("%lld", &n);

    long long ct = 0;
    long long m5;
    for (m5 = n / 5; m5 > 0; m5 /= 5)
    {
        ct += m5;
    }

    printf("Output: %lld\n", ct);
}

void prob3(void)
{
    long long n;
    printf("Enter number: ");
    scanf("%lld", &n);

    long long *factors = NULL;
    int *powers = NULL;
    int i1 = 0, capacity1 = 0;
    int i2 = 0, capacity2 = 0;

    printf("Output: ");
    int ct = 0;
    if (n % 2 == 0)
    {
        do
        {
            ct++;
            n /= 2;
        } while (n % 2 == 0);

        add_to_ll_arr(&factors, 2, &i1, &capacity1);
        add_to_int_arr(&powers, ct, &i2, &capacity2);
    }
    long long sqrtn = llSqrt(n);
    long long i;
    for (i = 3; i <= sqrtn; i += 2)
    {
        if (n % i == 0)
        {
            ct = 0;
            do
            {
                ct++;
                n /= i;
            } while (n % i == 0);
            sqrtn = llSqrt(n);

            add_to_ll_arr(&factors, i, &i1, &capacity1);
            add_to_int_arr(&powers, ct, &i2, &capacity2);
        }
    }
    if (n != 1)
    {
        add_to_ll_arr(&factors, n, &i1, &capacity1);
        add_to_int_arr(&powers, 1, &i2, &capacity2);
    }
    if (i1 == 0)
    {
        printf("1^1\n");
    }
    else
    {
        int i;
        for (i = 0; i < i1 - 1; i++)
        {
            printf("%lld^%d x ", factors[i], powers[i]);
        }
        printf("%lld^%d\n", factors[i], powers[i]);
    }
    free(factors);
    free(powers);

}
 
void prob4(void)
{
    int n;
    printf("Enter number: ");
    scanf("%d", &n);

    if (n == 0)
    {
        printf("{}");
        return;
    }

    int bits[n];
    int i;
    for (i = 0; i < n; i++)
    {
        bits[i] = 0;
    }
    while (!all_bits_one(bits, n))
    {
        print_bits(bits, n);
        printf(", ");
        for (i = 0; bits[i]; i++)
        {
            bits[i] = 0;
        }
        bits[i] = 1;
    }
    print_bits(bits, n);
    printf("\n");
}

void prob5(void)
{
    int n;
    int ch;

    int isquit;
    int capacity;

    isquit = 0;
    capacity = 0;
    int *arr1 = NULL;
    int num1 = 0;
    printf("Input first polynomial: ");
    while (!isquit)
    {
        scanf("%d", &n);
        add_to_int_arr(&arr1, n, &num1, &capacity);
        while ((ch = getchar()) != ',')
        {
            if (ch == '\n')
            {
                isquit = 1;
                break;
            }
        }
    }

    isquit = 0;
    capacity = 0;
    int *arr2 = NULL;
    int num2 = 0;
    printf("Input second polynomial: ");
    while (!isquit)
    {
        scanf("%d", &n);
        add_to_int_arr(&arr2, n, &num2, &capacity);
        while ((ch = getchar()) != ',')
        {
            if (ch == '\n')
            {
                isquit = 1;
                break;
            }
        }
    }

    printf("Output: ");
    int i;
    for (i = 0; i < num1 + num2 - 2; i++)
    {
        int sum = 0;
        int j, k;
        for (j = i, k = 0; j >= 0 && k < num2; j--, k++)
        {
            if (j < num1)
            {
                sum += arr1[j] * arr2[k];
            }
        }
        printf("%d, ", sum);
    }
    printf("%d\n", arr1[num1 - 1] * arr2[num2 - 1]);

    free(arr1);
    free(arr2);
}

void prob6(void)
{
    long long n;
    printf("Enter number: ");
    scanf("%lld", &n);

    printf("Output: The lightbulb is ");
    long long sqrtn = llSqrt(n);
    if (sqrtn * sqrtn == n)
        printf("off.\n");
    else
        printf("on.\n");
}
 
void prob7(void)
{
    long long n;
    printf("Enter number: ");
    scanf("%lld", &n);

    long long i;
    for (i = 1; i < n - 1; i++)
    {
        if (llGcd(n, i) == 1)
        {
            printf("%lld/%lld, ", i, n);
        }
    }
    printf("%lld/%lld\n", i, n);
}

/*
 * kth element of nth row = (n k)
 * (n k) = n!/(k!(n-k)!)
 * (n k) = n!/(k(k-1)!(1/(n-k+1))(n-k+1))!)
 * (n k) = n!/((k-1)!(n-k+1)!) * (n-k+1)/k
 * (n k) = (n k-1) * (n-k+1)/k
 */
void prob9(void)
{
    int n;
    printf("Enter number: ");
    scanf("%d", &n);

    printf("Output: ");
    int k;
    long long nk = 1;
    int isoverflow = 0;
    for (k = 1; k <= n; k++)
    {
        printf("%lld, ", nk);
        int p = n - k + 1;
        int q = k;
        // the next 3 lines are only for overflow protection
        int pqGcd = llGcd(p, q);
        p /= pqGcd;
        q /= pqGcd;
        nk /= q;
        nk *= p;
        // this is for overflow protection as well
        if (nk < 0)
        {
            printf("\nOVERFLOW ERROR\n");
            isoverflow = 1;
            break;
        }
    }
    if (!isoverflow)
    {
        printf("%lld\n", nk);
    }
}

void prob11(void)
{
    double input;
    printf("Enter number: ");
    scanf("%lf", &input);

    int num = round(input * 1000);
    int den = 1000;
    int gcd = llGcd(num, den);
    num /= gcd;
    den /= gcd;

    printf("Output: %d/%d = ", num, den);
    node *ans = egypt_brute_force(num, den);
    node *ptr = ans;
    if (ptr != NULL)
    {
        for (; ptr->next != NULL; ptr = ptr->next)
            printf("1/%lld + ", ptr->val);
        printf("1/%lld\n", ptr->val);
    }
    else
    {
        printf("No solution found (possible overflow)\n");
    }

    free_list(ans);
}
 
void prob12(void)
{
    double input;
    printf("Enter number:\n");
    scanf("%lf", &input);
    long long n = round(input * 100);

    int denom[] = {100000, 50000, 20000, 10000, 5000, 2000, 1000,
                    500, 100, 25, 10, 5, 1};
    int isfirstterm = 1;
    int i;
    for (i = 0; n > 0; i++)
    {
        long long div = n / denom[i];
        if (div > 0)
        {
            if (isfirstterm)
                isfirstterm = 0;
            else
                printf(" + ");
            printf("(%lld)%.2lf", div, (double) denom[i] / 100);
        }
        n %= denom[i];
    }
    printf("\n");
}
 
void prob14(void)
{
    long long n;
    printf("Enter number: ");
    scanf("%lld", &n);
    int b;
    printf("Enter base: ");
    scanf("%d", &b);

    char digits[36];
    int i;
    for (i = 0; i < 10; i++)
    {
        digits[i] = i + '0';
    }
    for (i = 10; i < 36; i++)
    {
        digits[i] = i - 10 + 'A';
    }

    char converted[sizeof(long long) * 8]; // longest possible resulting number
    for (i = 0; n != 0; i++)
    {
        converted[i] = digits[n % b];
        n /= b;
    }

    printf("Output: ");
    if (i == 0) printf("0");
    for (i -= 1; i >= 0; i--)
    {
        printf("%c", converted[i]);
    }
    printf("\n");
}

long long llGcd(long long a, long long b)
{
    long long x;
    if (a < b)
    {
        x = a;
        a = b;
        b = x;
    }
    while (b)
    {
        x = a % b;
        a = b;
        b = x;
    }
    return a;
}

long long llSqrt(long long n)
{
    if (n == 0) return 0;
    long long root = 1;
    while (root * root <= n)
    {
        root <<= 1;
    }
    root >>= 1;
    long long i = root >> 1;
    while (i > 0)
    {
        long long sq = root + i;
        sq *= sq;
        if (sq <= n)
        {
            root += i;
            if (sq == n)
                return root;
        }
        i >>= 1;
    }
    return root;
}

void add_to_int_arr(int **arr, int n, int *i, int *capacity)
{
    if (*i + 1 > *capacity)
    {
        if (*capacity == 0)
            *capacity = 8;
        else
            *capacity *= 2;
        *arr = realloc(*arr, *capacity * sizeof(int));
    }
    (*arr)[(*i)++] = n;
}
void add_to_ll_arr(long long **arr, long long n, int *i, int *capacity)
{
    if (*i + 1 > *capacity)
    {
        if (*capacity == 0)
            *capacity = 8;
        else
            *capacity *= 2;
        *arr = realloc(*arr, *capacity * sizeof(long long));
    }
    (*arr)[(*i)++] = n;
}

// Stewart 1992 (works under assumption that p/q in lowest terms
node *egypt_brute_force(long long p, long long q)
{
    node *head = NULL;
    if (p == 1)
    {
        head = add_to_top(head, q);
        return head;
    }
    int i;
    if ((head = egypt_two_terms(p, q))== NULL)
    {
        int k = 1;
        int foundSoln = 0;
        int overflowReached = 0;
        while (!foundSoln && !overflowReached)
        {
            long long uList[k];
            long long uMax[k];
            long long pList[k + 1];
            long long qList[k + 1];
            pList[0] = p;
            qList[0] = q;
            for (i = 0; i < k; i++)
            {
                uList[i] = qList[i] / pList[i] + 1;
                uMax[i] = qList[i] * (k + 2 - i) / pList[i];
                pList[i + 1] = pList[i] * uList[i] - qList[i];
                qList[i + 1] = qList[i] * uList[i];
                long long pqGcd = llGcd(pList[i + 1], qList[i + 1]); 
                pList[i + 1] /= pqGcd; qList[i + 1] /= pqGcd;
            }
            while (1)
            {
                if ((head = egypt_two_terms(pList[k], qList[k])) != NULL)
                {
                    if (head->val == 0)
                    {
                        free_list(head);
                        head = NULL;
                        overflowReached = 1;
                        break;
                    }
                    else
                    {
                        for (i = k - 1; i >= 0; i--)
                        {
                            head = add_to_top(head, uList[i]);
                        }
                    }
                    foundSoln = 1;
                    break;
                }
                int carry = 1;
                int reset = k;
                for (i = k - 1; i >= 0; i--)
                {
                    if (carry == 1)
                    {
                        uList[i]++;
                    }
                    if (uList[i] <= uMax[i])
                    {
                        carry = 0;
                        reset = i;
                        break;
                    }
                }
                if (carry == 1)
                {
                    break;
                }
                for (i = reset + 1; i < k; i++)
                {
                    pList[i] = pList[i - 1] * uList[i - 1] - qList[i - 1];
                    qList[i] = qList[i - 1] * uList[i - 1];
                    long long pqGcd = llGcd(pList[i], qList[i]); 
                    pList[i] /= pqGcd; qList[i] /= pqGcd;

                    uList[i] = qList[i] / pList[i] + 1;
                    uMax[i] = qList[i] * (k + 2 - i) / pList[i];
                }
                pList[k] = pList[k - 1] * uList[k - 1] - qList[k - 1];
                qList[k] = qList[k - 1] * uList[k - 1];
                long long pqGcd = llGcd(pList[k], qList[k]); 
                pList[k] /= pqGcd; qList[k] /= pqGcd;
            }
            k++;
        }
    }
    return head;
}

node *egypt_two_terms(long long p, long long q)
{
    node *head = NULL;
    long long qsquared = q * q;
    if (qsquared < 0)
    {
        head = add_to_top(head, 0);
        return head;
    }
    long long i;
    for (i = 1; i < q; i++)
    {
        if ((i + q) % p == 0 && qsquared % i == 0)
        {
            head = add_to_top(head, (qsquared / i + q) / p);
            head = add_to_top(head, (i + q) / p);
            break;
        }
    }
    return head;
}


node *add_to_top(node *head, long long val)
{
    node *newHead = malloc(sizeof(node));
    newHead->val = val;
    newHead->next = head;
    return newHead;
}

void free_list(node *head)
{
    node *ptr = head;
    while (ptr != NULL)
    {
        head = ptr->next;
        free(ptr);
        ptr = head;
    }
}

int all_bits_one(int bits[], int n)
{
    int i;
    for (i = 0; i < n; i++)
        if (!bits[i])
            return 0;
    return 1;
}

void print_bits(int bits[], int n)
{
    int i;
    int isFirstElem = 1;
    printf("{");
    for (i = 0; i < n; i++)
    {
        if (bits[i])
        {
            if (isFirstElem)
            {
                isFirstElem = 0;
            }
            else
            {
                printf(", ");
            }
            printf("%d", i + 1);
        }
    }
    printf("}");
}
